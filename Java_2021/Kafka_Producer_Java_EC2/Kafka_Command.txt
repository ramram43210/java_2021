zookeeper-server-start.sh /home/ec2-user/kafka_2.13-3.3.1/config/zookeeper.properties
kafka-server-start.sh /home/ec2-user/kafka_2.13-3.3.1/config/server.properties

kafka-topics.sh --bootstrap-server ec2-3-83-93-46.compute-1.amazonaws.com:8082 --topic AnimalTopic --create --partitions 3 --replication-factor 1
kafka-console-consumer.sh --bootstrap-server ec2-3-83-93-46.compute-1.amazonaws.com:8082 --topic AnimalTopic
